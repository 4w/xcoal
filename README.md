# This not officially released Minetest mod was moved

The repository was moved away from GitLab. The new location can be found here:

* https://git.0x7be.net/dirk/xcoal

Just set the new origin in your local copy of the repository.

Also make sure to change `master` branch to `main` branch. The `master` branch is no longer in use and was renamed.
